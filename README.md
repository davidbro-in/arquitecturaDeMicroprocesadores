# Arquitectura De Microprocesadores
Espacio de trabajo para la materia Arquitectura de Microprocesadores de la Especialización de Sistemas Embebidos de FIUBA

## Uso de este repositorio
1. Descargar [*software* de CIAA](https://github.com/epernia/software/releases)
1. Descargar [firmware_v3 de CIAA](https://github.com/epernia/firmware_v3/releases)
1. [Configurar Eclipse](https://github.com/epernia/software/blob/master/eclipse/docs/configurar_eclipse.md)  y asegurarse de [configurar *debug*](https://github.com/epernia/software/blob/master/eclipse/docs/configurar_eclipse.md#configuraci%C3%B3n-de-la-descarga-y-depuraci%C3%B3n-del-programa-sobre-el-hardware)
1. Crear una carpeta llamada `_programs` (o el nombre que desee) dentro de la carpeta del [firmware_v3 de CIAA](https://github.com/epernia/firmware_v3/releases)
1. Descargar este repositorio dentro de la carpeta creada en el punto anterior (`_programs`)
1. Si el [firmware_v3 de CIAA](https://github.com/epernia/firmware_v3/releases) se instaló en `/home/$USER/CIAA/firmware_v3`, ejecutar:
```sh
cd /home/$USER/CIAA/firmware_v3/_programs/arquitecturaDeMicroprocesadores
make patch
```
Se puede leer más sobre la ejecución de *targets* en la [documentación](https://github.com/epernia/firmware_v3/blob/master/documentation/firmware/readme/readme-es.md#utilizaci%C3%B3n-de-firmware_v3) de firmaware_v3
En Eclipse también se pueden crear *build targets* siguiendo estas [instrucciones](https://github.com/epernia/firmware_v3/blob/master/documentation/firmware/eclipse/usage-es.md#32-configurar-targets-de-de-makefile-en-eclipse), en cuyo caso se puede crear `patch`

**NOTA:** Este último paso se ejecuta sólo una vez!! (Es porque el `Makefile` original de firmware_v3 no permite extender la variable OBJECTS)

7. Ejecutar `make select_program`, `make all` y todos los comandos habituales de firmware_v3
