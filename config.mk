# Compile options
VERBOSE=n
OPT=g
USE_NANO=y
SEMIHOST=n
USE_FPU=y

ASSRC+=$(wildcard $(PROGRAM_PATH_AND_NAME)/src/*.S)
ASSRC+=$(foreach m, $(MODULES), $(wildcard $(m)/src/*.S))

OBJECTS+=$(ASSRC:%.S=$(OUT)/%.o)

$(OUT)/%.o: %.S
	@echo AS $<
	@mkdir -p $(dir $@)
	$(Q)$(CC) -MMD $(CFLAGS) -c -o $@ $<
	
patch:
	git apply $(PROGRAM_PATH_AND_NAME)/0001-Append-OBJECTS-to-previous-value.patch
